#!/bin/bash
# Generates files with cmake and builds the makefile

echo "BUILDING..."

if [ -z $(command -v cmake) ] || [ -z $(command -v avr-gcc) ] || [ -z $(command -v avr-g++) ] || [ -z $(command -v avr-ld) ] || [ -z $(command -v avr-ar) ]
then
    echo "Installing dependencies..."
    echo "Updating..."
    apt-get update
    apt-get upgrade -y
    echo "Installing Make tools..."
    apt-get -y install cmake make
    echo "Installing AVR toolchain..."
    apt-get -y install gcc-avr binutils-avr avr-libc
    echo "Done installing dependencies."
fi

PARENT=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
cd "$PARENT/.."

if [ ! -d build ]
then
    mkdir build
fi

cd build
cmake ..
make $@
