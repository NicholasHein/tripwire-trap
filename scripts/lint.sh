#!/bin/bash
# Lints the source code and checks for errors

echo "LINTING..."

if [ -z $(command -v cppcheck) ]
then
    echo "Installing dependencies..."
    echo "Updating..."
    apt-get update
    apt-get upgrade all
    echo "Installing linter..."
    apt-get -y install cppcheck
    echo "Done installing dependencies."
fi

PARENT=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
cppcheck "$PARENT/../src" -I "$PARENT/../include" --enable=warning,style,performance,portability,information,missingInclude --error-exitcode=1 --suppress=missingIncludeSystem
