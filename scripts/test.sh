#!/bin/bash
# Runs unit and system tests on the source code and compiles results

echo "TESTING..."

if [ -z "$(ldconfig -p | grep libcheck)" ]
then
    echo "Installing dependencies..."
    echo "Updating..."
    apt-get update
    apt-get upgrade all
    echo "Installing check..."
    apt-get -y install check
    echo "Done installing dependencies."
fi

PARENT=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
LIBNAME=$(basename $(dirname "../$PARENT"))

if [ -z $(find "$PARENT/../bin" -iregex ".*\\.a") ]
then
    bash "$PARENT/../build.sh"
fi

mkdir -p "$PARENT/../bin/tests"

for file in $(ls "$PARENT/../tests/*.c" 2>/dev/null)
do
    echo "Generating test ${file%.*}..."
    gcc "$PARENT/../tests/$file" -lcheck -lm -lpthread -lrt -L"$PARENT/../bin" -l$LIBNAME -I"$PARENT/../include" -o "$PARENT/../bin/tests/${file%.*}"
    echo "Running test ${file%.*}..."
    $PARENT/../bin/tests/${file%.*}
done

# Build as library, import library during test builds, execute tests, compile results, return exit code
# Writing and building:
# http://www.ccs.neu.edu/home/skotthe/classes/cs5600/fall/2015/labs/intro-check.html
# Logging:
# http://check.sourceforge.net/doc/check_html/check_4.html#Test-Logging
