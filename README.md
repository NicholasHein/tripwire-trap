# Tripwire Trap

The tripwire trap project is firmware for an ATtiny85 system that monitors
a laser tripwire system, and, when armed, triggers a capacitor bank to discharge
a large amount of current all at once.

## The System

The system is separated into multiple individual modules: the control, trigger,
charge, and power modules.

### Control

The system is controlled with an ATtiny85 which is able to operate under very
low power, has an ADC, and has digital IO pins.  The ATtiny85 is interfaced with
a push button and a piezzo buzzer.  The push button is used to set the mode, and
the piezzo buzzer reflects the status of the system.  When the system is
triggered, the piezzo activates periodically in "recovery mode".

### Trigger

The trigger system consists of a laser and photo-resistor.  The laser is
activated when the system enters "setup mode", and it must be oriented to shine
at the photo-resistor.  The analog current level received from the
photo-resistor is also calibrated in "setup mode".  When the photo-resistor
is calibrated, the system transitions to "armed mode".  Moderate fluctuation in
the analog level triggers "discharge mode".

### Charge

The charge system is the active component of the system.  It consists of relay
system controlling a Marx generator.  Before "armed mode," the Marx generator
remains in a discharged state.  Then, when the system is in "armed mode," the
relay system begins charging the Marx generator.  When the trigger system is
tripped, the system transitions from "armed mode" to "discharge mode" where the
Marx generator is discharged producing a large amount of current across the
payload.  The system then falls into "recovery mode" after a period of time.

Reference: [High power pulse generator based on avalanche transistor Marx circuit (paper)](https://ieeexplore.ieee.org/document/7062282)

### Power

The system is powered with a battery bank >5V which is fed into an LM317T for
voltage regulation for the micro-controller and is connected directly to the
Marx generator.

## Modes

The system progresses linearly through a series of modes consisting of setup,
calibration, armed, discharge, and recovery modes.

 1. Setup
    - **Power on**
    - *Three beeps*
    - *Run setup*
    - *Await user input*
    - **Button press & release**
 2. Calibration
    - *One beep*
    - *Laser on*
    - Set up laser and receiver positions
    - **Button press & release**
    - *Calibrate analog levels*
    - *Validation*
      - Success: *One beep*
      - Failure: *Five beeps* (try again)
    - **Button hold 3s & release**
 3. Armed
    - *Four beeps*
    - *One long beep*
    - *Await trigger*
    - **Trigger**
 4. Discharge
    - *Discharge Marx generator*
 5. Recovery
    - *Beep every 30s*
