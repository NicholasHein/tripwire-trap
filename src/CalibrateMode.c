
#include "CalibrateMode.h"
#include "Configuration.h"
#include "Sleep.h"
#include "Stopwatch.h"
#include "components/Button.h"
#include "components/Piezzo.h"
#include "components/Sensor.h"
#include <stdint.h>
#include <stdlib.h>

static void start_calibration (void);
static void stop_calibration (void);
static void button_down_arm (void);
static void button_up_arm (void);

static volatile calibration_status_t status = CALIBRATE_NONE;


void run_calibrate_mode (void)
{
    // 1. One beep
    piezzo_alert(CALIBRATE_ON_BEEPCOUNT);
    // 2. Laser On
    sensor_on();

    double val = 0.0;
    while (val < TRIGGER_MINIMUM) {
        // 3. Await button press
        button_set_down_handler(&start_calibration);
        power_idle();
        button_set_down_handler(NULL);
        button_set_up_handler(&stop_calibration);

        sensor_start();

        while (status == CALIBRATE_INVALIDATED) {
            val = sensor_measure();
            if (val > TRIGGER_MINIMUM) {
                piezzo_alert(1);
            }
        }
        button_set_up_handler(NULL);
        sensor_stop();

        if (status == CALIBRATE_VALIDATING) {
            val = sensor_measure();
            if (val < TRIGGER_MINIMUM) {
                piezzo_alert(CALIBRATE_INVALID_BEEPCOUNT);
                status = CALIBRATE_NONE;
            } else {
                status = CALIBRATE_VALIDATED;
            }
        }
    }
    // 5. Calibrate analog levels
    piezzo_alert(CALIBRATE_VALID_BEEPCOUNT);
    sensor_set_bounds(TRIGGER_FACTOR * val, 1.0);
    // 6. Await button hold
    while (status != CALIBRATE_ARMED_SUCCESS) {
        button_set_down_handler(&button_down_arm);
        power_idle();
        button_set_down_handler(NULL);
        button_set_up_handler(&button_up_arm);
        while (status == CALIBRATE_AWAITING) ;
        button_set_up_handler(NULL);
    }
}

static void start_calibration (void)
{
    status = CALIBRATE_INVALIDATED;
    power_wake();
}

static void stop_calibration (void)
{
    status = CALIBRATE_VALIDATING;
    power_wake();
}

static void button_down_arm (void)
{
    stopwatch_start();
    status = CALIBRATE_AWAITING;
    power_wake();
}

static void button_up_arm (void)
{
    double t = stopwatch_stop();
    if (t >= CALIBRATE_ARM_BUTTONPRESSTIME) {
        status = CALIBRATE_ARMED_SUCCESS;
    } else {
        status = CALIBRATE_ARMED_FAILED;
    }
}
