
#include "Configuration.h"

#include "SetupMode.h"
#include "CalibrateMode.h"
#include "ArmedMode.h"
#include "DischargeMode.h"
#include "RecoveryMode.h"

#include "components/MarxGenerator.h"
#include <util/delay.h>

int main (void)
{
    run_setup_mode();
    run_calibrate_mode();
    run_armed_mode();
    run_discharge_mode();
    run_recovery_mode();
    return 0;
}
