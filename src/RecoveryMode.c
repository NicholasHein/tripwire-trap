
#include "RecoveryMode.h"
#include "Configuration.h"
#include "Sleep.h"
#include "Stopwatch.h"
#include "components/Piezzo.h"

static void recovery_handler (void);

void run_recovery_mode (void)
{
    stopwatch_enable_ps();
    while (1) {
	timer_t *timer = stopwatch_add(RECOVERY_BEEP_PERIOD, &recovery_handler);
        stopwatch_start();
        power_idle();
        stopwatch_stop();
        stopwatch_remove(timer);
        piezzo_short_beep();
    }
    while (1) ;
}

static void recovery_handler (void)
{
    power_wake();
}
