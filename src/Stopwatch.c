
#include "Stopwatch.h"
#include "Configuration.h"
#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdint.h>
#include <stdlib.h>
#include "components/Piezzo.h"

static void stopwatch_init (void);
static stopwatch_status_t status = STOPWATCH_NONE;
uint8_t ps_enabled = 0;

static double increment = 0.0;
static volatile double stopwatch_value = 0.0;

static timer_t **timer_ptrs;
static uint16_t timer_count = 0;

static void stopwatch_init (void)
{
    if (status != STOPWATCH_NONE) {
        return;
    }

    TCCR1 |= (1 << CTC1);
    if (ps_enabled) {
        TCCR1 |= (0xF & STOPWATCH_CS_LOW_FLAGS);
        OCR1C = STOPWATCH_CS_LOW_CMPVAL;
    } else {
        TCCR1 |= (0xF & STOPWATCH_CS_HIGH_FLAGS);
        OCR1C = STOPWATCH_CS_HIGH_CMPVAL;
    }
    GTCCR |= (1 << PSR1);

    // TODO: reset timer data register here
    // TODO: fix stopwatch references in other files
    // TODO: copy dev-ops, build files, and everything

    stopwatch_disable_ps();
    sei();
    status = STOPWATCH_INITIALIZED;
}

void stopwatch_start (void)
{
    stopwatch_init();
    stopwatch_value = 0.0;
    status = STOPWATCH_STARTED;
    TIMSK |= (1 << OCIE1A);
}

timer_t *stopwatch_add (
    double time,
    stopwatch_callback_t callback
) {
    if (time < 0)
        return NULL;

    stopwatch_init();
    timer_t *timer = malloc(sizeof(timer_t));
    if (!timer)
        return NULL;
    timer->time = time;
    timer->callback = callback;
    timer->status = 0x0;

    timer_t **temp = realloc(
        timer_ptrs,
        ++timer_count * sizeof(timer_t *)
    );
    if (!temp)
    {
        free(timer);
        timer_count--;
        free(temp);
        return NULL;
    } else {
        timer_ptrs = temp;
        timer_ptrs[timer_count - 1] = timer;
    }

    return timer;
}

uint8_t stopwatch_remove (timer_t *timer)
{
    for (uint16_t i = 0; i < timer_count; i++) {
        if (timer_ptrs[i] == timer) {
            timer_ptrs[i] = timer_ptrs[timer_count - 1];
            timer_t **temp = realloc(
                timer_ptrs,
                --timer_count * sizeof(timer_t *)
            );
            if (!temp) {
                free(temp);
                timer_count++;
                return 0;
            } else {
                timer_ptrs = temp;
            }

            return 1;
        }
    }
    return 0;
}

void stopwatch_enable_ps (void)
{
    increment = ((double)(STOPWATCH_CS_LOW_PRESCALE) / (double)(F_CPU))
                    * (double)(STOPWATCH_CS_LOW_CMPVAL);
    ps_enabled = 1;
}

void stopwatch_disable_ps (void)
{
    increment = ((double)(STOPWATCH_CS_HIGH_PRESCALE) / (double)(F_CPU))
                    * (double)(STOPWATCH_CS_HIGH_CMPVAL);
    ps_enabled = 0;
}

double stopwatch_time (void)
{
    if (status != STOPWATCH_NONE) {
        return stopwatch_value;
    } else {
        return 0.0;
    }
}

double stopwatch_stop (void)
{
    if (status == STOPWATCH_NONE) {
        return 0.0;
    }

    double temp = stopwatch_value;
    stopwatch_value = 0.0;
    TIMSK &= ~(1 << OCIE1A);

    status = STOPWATCH_INITIALIZED;
    return temp;
}

ISR(TIMER1_COMPA_vect)
{
    if (status == STOPWATCH_STARTED) {
        stopwatch_value += increment;
        for (uint16_t i = 0; i < timer_count; i++) {
            timer_t *timer = timer_ptrs[i];
            if (timer->status == 0x0) {
                if (timer->time <= stopwatch_value) {
                    timer->status = 0x1;
                    timer->callback();
                }
            }
        }
    }
}
