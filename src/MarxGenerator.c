
#include "components/MarxGenerator.h"
#include "Configuration.h"
#include <util/delay.h>
#include <stdlib.h>

void marx_init (void)
{
    // Set register direction
    DDRX_MARXGEN |= MASK_MARXGEN;
    // Set as pull-up resistors
    PORT_MARXGEN &= ~MASK_MARXGEN;
}

void marx_discharge (void)
{
    // Sets pull-up resistors
    PORT_MARXGEN |= MASK_MARXGEN;
    _delay_ms(DISCHARGE_PERIOD);
    // Sets pull-down resistors
    PORT_MARXGEN &= ~MASK_MARXGEN;
}
