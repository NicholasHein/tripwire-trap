
#include "ArmedMode.h"
#include "Configuration.h"
#include "Sleep.h"
#include "components/Piezzo.h"
#include "components/MarxGenerator.h"
#include "components/Sensor.h"
#include <stdlib.h>

static void sensor_triggered (double val);

static volatile uint8_t is_blocked = 0;
static volatile uint8_t is_triggered = 0;

void run_armed_mode (void)
{
    // 1. Four beeps and one long beep
    piezzo_alert(ARMED_ON_BEEPCOUNT);
    piezzo_long_beep();
    // 5. Await trigger
    sensor_set_bound_handler(&sensor_triggered);
    sensor_start();
    while (!is_triggered) {
        power_idle();
    }

    sensor_stop();
    sensor_set_bound_handler(NULL);
    sensor_off();
}

static void sensor_triggered (double val)
{
    if (is_blocked == 0) {
        is_blocked = 1;
    } else {
        is_blocked = 0;
        is_triggered = 1;
        power_wake();
    }
}
