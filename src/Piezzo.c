
#include "components/Piezzo.h"
#include "Configuration.h"
#include <util/delay.h>

void piezzo_init (void)
{
    // Set register direction
    DDRX_PIEZZO |= MASK_PIEZZO;
    // Set as pull-down resistors
    PORT_PIEZZO &= ~MASK_PIEZZO;
}

void piezzo_alert (uint8_t times)
{
    for (uint8_t i = 0; i < times; i++) {
        piezzo_short_beep();
        _delay_ms(PIEZZO_SHORT_RES_OFF);
    }
}

void piezzo_short_beep (void)
{
    // Set as pull-up resistors
    PORT_PIEZZO |= MASK_PIEZZO;
    _delay_ms(PIEZZO_SHORT_RES_ON);
    // Set as pull-down resistors
    PORT_PIEZZO &= ~MASK_PIEZZO;
}

void piezzo_long_beep (void)
{
    // Set as pull-up resistors
    PORT_PIEZZO |= MASK_PIEZZO;
    _delay_ms(PIEZZO_LONG_RES_ON);
    // Set as pull-down resistors
    PORT_PIEZZO &= ~MASK_PIEZZO;
}
