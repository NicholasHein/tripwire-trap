
#include "components/Button.h"
#include <avr/interrupt.h>
#include <avr/io.h>
#include <stdlib.h>

static button_handler fall_handler = 0;
static button_handler rise_handler = 0;

static void check_interrupt (void);

void button_init (void)
{
    // Set pin direction as input
    DDRX_BUTTON &= ~MASK_BUTTON;
    // Set pull-up resistor
    PORT_BUTTON |= MASK_BUTTON;
    // Enable interrupt 0
    GIMSK |= (1 << INT0);
    // Trigger interrupt 0 on a rising and falling edge
    MCUCR |= (1 << ISC00);
    MCUCR &= ~(1 << ISC01);
    // Enable interrupts
    sei();
}

uint8_t button_status (void)
{
    return (PINX_BUTTON & MASK_BUTTON) == 0;
}

void button_set_up_handler (button_handler handler)
{
    rise_handler = handler;
    check_interrupt();
}

void button_set_down_handler (button_handler handler)
{
    fall_handler = handler;
    check_interrupt();
}

static void check_interrupt (void)
{
    if (rise_handler == NULL && fall_handler == NULL) {
        // Disable interrupt 0
        GIMSK &= ~(1 << INT0);
    } else {
        // Enable interrupt 0
        GIMSK |= (1 << INT0);
    }
}

ISR(INT0_vect)
{
    if (button_status()) {
        if (fall_handler != NULL) {
            fall_handler();
        }
    } else {
        if (rise_handler != NULL) {
            rise_handler();
        }
    }
}
