
#include "components/Sensor.h"
#include "components/ADC.h"
#include <stdlib.h>
#include "components/Piezzo.h"


void sensor_init (void)
{
    // Sets register direction for output
    DDRX_SENSORLSR |= MASK_SENSORLSR;
}

void sensor_on (void)
{
    // Sets as pull-up resistors
    PORT_SENSORLSR |= MASK_SENSORLSR;
}

void sensor_off (void)
{
    // Sets as pull-down resistors
    PORT_SENSORLSR &= ~MASK_SENSORLSR;
}


static void adc_handler (double val);
static adc_listener_t *listener;
static sensor_bound_handler_t bound_handler;

static double low_charge = 0.0;
static double high_charge = 1.0;
static volatile double current_charge = -1.0;

double sensor_measure (void)
{
    return current_charge;
}

void sensor_start (void)
{
    listener = adc_start_listener(
        &DDRX_SENSOR,
        MASK_SENSOR,
        MUXX_SENSOR,
        &adc_handler
    );
}

void sensor_stop (void)
{
    adc_stop_listener(listener);
    free(listener);
}

void sensor_set_bound_handler (sensor_bound_handler_t handler)
{
    bound_handler = handler;
}

void sensor_set_bounds (double low, double high)
{
    if (low > high) {
        return;
    }
    if (low >= 0.0 && low <= 1.0) {
        low_charge = low;
    }
    if (high >= 0.0 && high <= 1.0) {
        high_charge = high;
    }
}

static void adc_handler (double val)
{
    double previous = current_charge;
    current_charge = val;
    if (previous < 0 || current_charge < 0) {
        return;
    }
    if ((previous <= low_charge && val > low_charge) ||
        (previous >= low_charge && val < low_charge) ||
        (previous <= high_charge && val > high_charge) ||
        (previous >= high_charge && val < high_charge))
    {
        if (bound_handler != NULL) {
            bound_handler(val);
        }
    }
}
