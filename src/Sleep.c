
#include "Sleep.h"
#include <avr/io.h>
#include <avr/sleep.h>

void power_down (void)
{
    set_sleep_mode(SLEEP_MODE_PWR_DOWN);
    sleep_enable();
    sleep_cpu();
}

void power_idle (void)
{
    set_sleep_mode(SLEEP_MODE_IDLE);
    sleep_enable();
    sleep_cpu();
}

void power_sleep (void)
{
    set_sleep_mode(SLEEP_MODE_ADC);
    sleep_enable();
    sleep_cpu();
}

void power_wake (void)
{
    sleep_disable();
}
