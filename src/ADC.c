
#include "components/ADC.h"
#include <avr/interrupt.h>
#include <avr/io.h>
#include <stdint.h>
#include <stdlib.h>

static uint8_t status = 0;
static volatile uint16_t reg_index = 0;

static adc_listener_t **active_reg_ptrs;
static uint16_t active_regs_count = 0;

static void restart_cycle (void);

void adc_init (void)
{
    if (status != 0) {
        return;
    }
    // ADEN: enable ADC module
    // ADIE: enable ADC interrupt
    // ADPSx: set ADC prescaler to 128
    ADCSRA |= (1 << ADEN);
    ADCSRA |= (1 << ADATE);
    ADCSRA |= (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0);
    // Enable interrupts
    sei();
    status = 1;
}

adc_listener_t *adc_start_listener (
    volatile uint8_t *reg,
    uint8_t mask,
    uint8_t mux_flags,
    adc_callback_t callback
) {
    if (mask == 0)
        return NULL;
    if (mux_flags & 0xF0)
        return NULL;

    adc_init();
    *reg &= ~mask;

    adc_listener_t *listener = malloc(sizeof(adc_listener_t));
    if (!listener)
        return NULL;
    listener->mux = mux_flags;
    listener->callback = callback;

    adc_listener_t **temp = realloc(
        active_reg_ptrs,
        ++active_regs_count * sizeof(adc_listener_t *)
    );
    if (!temp) {
        free(temp);
        active_regs_count--;
        free(listener);
        return NULL;
    } else {
        active_reg_ptrs = temp;
        active_reg_ptrs[active_regs_count - 1] = listener;
    }

    restart_cycle();

    return listener;
}

uint8_t adc_stop_listener (adc_listener_t *adc_listener)
{
    for (uint16_t i = 0; i < active_regs_count; i++) {
        if (active_reg_ptrs[i] == adc_listener) {
            active_reg_ptrs[i] = active_reg_ptrs[active_regs_count - 1];
            adc_listener_t **temp = realloc(
                active_reg_ptrs,
                --active_regs_count * sizeof(adc_listener_t *)
            );
            if (!temp)
            {
                free(temp);
                active_regs_count++;
                return 0;
            } else {
                active_reg_ptrs = temp;
            }

            restart_cycle();
            return 1;
        }
    }
    return 0;
}

/**
 * Starts the ADC measure cycle at the first ADC listener
 */
static void restart_cycle (void)
{
    if (active_regs_count > 0) {
        adc_listener_t *first = active_reg_ptrs[0];
        // Select ADC pin
        ADMUX = (ADMUX & 0xF0) | first->mux;
        // Start conversions
        ADCSRA |= (1 << ADSC);
        ADCSRA |= (1 << ADIE);
    } else {
        // Stop conversions
        ADCSRA &= ~(1 << ADSC);
        ADCSRA &= ~(1 << ADIE);
    }
    reg_index = 0;
}

ISR(ADC_vect)
{
    uint16_t data = ADCW;
    double value = (double)data / (double)0x3FF;

    adc_listener_t *listener = active_reg_ptrs[reg_index];
    listener->callback(value);
    if (++reg_index >= active_regs_count) {
        reg_index = 0;
    }

    listener = active_reg_ptrs[reg_index];
    // Select ADC pin
    ADMUX = (ADMUX & 0xF0) | listener->mux;
}
