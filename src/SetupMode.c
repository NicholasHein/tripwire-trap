
#include "SetupMode.h"
#include "Configuration.h"
#include "Sleep.h"
#include "components/Button.h"
#include "components/MarxGenerator.h"
#include "components/Piezzo.h"
#include "components/Sensor.h"
#include <stdlib.h>

static void btn_down_handler (void);
static void btn_up_handler (void);

void run_setup_mode (void)
{
    // 1. Three beeps
    piezzo_init();
    piezzo_alert(SETUP_ON_BEEPCOUNT);
    // 2. Run setup
    button_init();
    marx_init();
    sensor_init();
    // 4. Await user input
    button_set_down_handler(&btn_down_handler);
    power_idle();
    button_set_down_handler(NULL);
    button_set_up_handler(&btn_up_handler);
    power_idle();
    button_set_up_handler(NULL);
}

static void btn_down_handler (void)
{
    power_wake();
}

static void btn_up_handler (void)
{
    power_wake();
}
