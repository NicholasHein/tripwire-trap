
#ifndef CALIBRATEMODE_H
#define CALIBRATEMODE_H

typedef enum {
    CALIBRATE_NONE,
    CALIBRATE_INVALIDATED,
    CALIBRATE_VALIDATING,
    CALIBRATE_VALIDATED,
    CALIBRATE_AWAITING,
    CALIBRATE_ARMED_FAILED,
    CALIBRATE_ARMED_SUCCESS
} calibration_status_t;

/**
 * Starts the calibrate mode sequence
 */
void run_calibrate_mode (void);

#endif
