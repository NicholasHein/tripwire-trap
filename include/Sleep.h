
#ifndef SLEEP_H
#define SLEEP_H

/**
 * Powers down the MCU
 */
void power_down (void);

/**
 * Idles the MCU
 */
void power_idle (void);

/**
 * Puts the MCU to sleep
 */
void power_sleep (void);

/**
 * Wakes up the MCU
 */
void power_wake (void);

#endif
