
#ifndef RECOVERYMODE_H
#define RECOVERYMODE_H

/**
 * Starts the recovery mode sequence
 */
void run_recovery_mode (void);

#endif
