
#ifndef ADC_H
#define ADC_H

#include <stdint.h>

typedef void (*adc_callback_t)(double val);

typedef struct {
    uint8_t mux;
    adc_callback_t callback;
} adc_listener_t;

/**
 * Initializes the ADC module.
 *  Note: must be done before any conversions.
 *  Note: done automatically when a conversion is started
 */
void adc_init (void);

/**
 * Start a listener on an ADC pin.
 *  Note: PORT and DD changes are persistent
 *  Note: returns NULL if invalid or memory error
 * @param  reg      The register hosting the ADC pin
 * @param  mask     The register mask for the ADC pin
 * @param  muxFlags Mux flags selecting the ADC pin
 * @param  callback The callback when a conversion is ready
 * @return          The pointer to the started ADC listener
 */
adc_listener_t *adc_start_listener (
    volatile uint8_t *reg,
    uint8_t mask,
    uint8_t mux_flags,
    adc_callback_t callback
);

/**
 * Stop a listener on an ADC pin.
 * @param index The pointer to the ADC listener to stop
 * @return      If successful
 */
uint8_t adc_stop_listener (adc_listener_t *adc_listener);



#endif
