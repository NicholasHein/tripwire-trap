
#ifndef SENSOR_H
#define SENSOR_H

#include <avr/io.h>

typedef void (*sensor_bound_handler_t)(double level);

#define DDRX_SENSORLSR DDRB
#define PORT_SENSORLSR PORTB
#define MASK_SENSORLSR (1 << PB0)

/**
 * Initializes the sensor
 */
void sensor_init (void);

/**
 * Turns the laser on
 */
void sensor_on (void);

/**
 * Turns the laser off
 */
void sensor_off (void);

#define DDRX_SENSOR DDRB
#define MASK_SENSOR (1 << PB3)
#define MUXX_SENSOR 0x3

/**
 * Measures the sensor's voltage level
 * @return  The voltage level as a percent of the reference voltage
 */
double sensor_measure (void);

/**
 * Starts sensor measurements
 */
void sensor_start (void);

/**
 * Stops sensor measurements
 */
void sensor_stop (void);

/**
 * Sets the callback for when a boundary is crossed
 * @param handler The callback function
 */
void sensor_set_bound_handler (void (*handler)(double));

/**
 * Sets the sensor boundary levels
 * @param low  The lower voltage boundary
 * @param high The upper voltage boundary
 */
void sensor_set_bounds (double low, double high);

#endif
