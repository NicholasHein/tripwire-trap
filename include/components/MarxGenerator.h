
#ifndef MARXGENERATOR_H
#define MARXGENERATOR_H

#include <avr/io.h>

#define DDRX_MARXGEN DDRB
#define PORT_MARXGEN PORTB
#define MASK_MARXGEN (1 << PB4)

/**
 * Initializes the marx generator
 */
void marx_init (void);

/**
 * Immediately triggers a marx generator discharge
 */
void marx_discharge (void);

#endif
