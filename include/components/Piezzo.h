
#ifndef PIEZZO_H
#define PIEZZO_H

#include <stdint.h>
#include <avr/io.h>

#define DDRX_PIEZZO DDRB
#define PORT_PIEZZO PORTB
#define MASK_PIEZZO (1 << PB1)

#define PIEZZO_SHORT_RES_OFF 200
#define PIEZZO_SHORT_RES_ON 100
#define PIEZZO_LONG_RES_ON 500

/**
 * Initializes the piezzo buzzer
 */
void piezzo_init (void);

/**
 * Beeps the piezzo a certain number of times
 * @param times The number of times to beep
 */
void piezzo_alert (uint8_t times);

/**
 * Beeps once a short time
 */
void piezzo_short_beep (void);

/**
 * Beeps once a long time
 */
void piezzo_long_beep (void);

#endif
