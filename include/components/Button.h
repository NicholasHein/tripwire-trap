
#ifndef BUTTON_H
#define BUTTON_H

#include <stdint.h>
#include <avr/io.h>

#define DDRX_BUTTON DDRB
#define PORT_BUTTON PORTB
#define PINX_BUTTON PINB
#define MASK_BUTTON (1 << PB2)

typedef void (*button_handler)(void);

/**
 * Initializes the button
 */
void button_init (void);

/**
 * Gets the button's status
 *  Note: value is 1 when down and 0 when up
 * @return  The status value
 */
uint8_t button_status (void);

/**
 * Sets the callback when the button goes up.
 * @param handler The callback function
 */
void button_set_up_handler (button_handler handler);

/**
 * Sets the callback when the button goes down.
 * @param down The callback function
 */
void button_set_down_handler (button_handler down);

#endif
