
#ifndef ARMEDMODE_H
#define ARMEDMODE_H

/**
 * Starts the armed mode sequence
 */
void run_armed_mode (void);

#endif
