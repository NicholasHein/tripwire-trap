
#ifndef SETUPMODE_H
#define SETUPMODE_H

/**
 * Starts the setup mode sequence
 */
void run_setup_mode (void);

#endif
