
#ifndef STOPWATCH_H
#define STOPWATCH_H

#include <stdint.h>

#define STOPWATCH_CS_HIGH_PRESCALE 1024
#define STOPWATCH_CS_HIGH_FLAGS 0xB
#define STOPWATCH_CS_HIGH_CMPVAL 1
#define STOPWATCH_CS_LOW_PRESCALE 8192
#define STOPWATCH_CS_LOW_FLAGS 0xE
#define STOPWATCH_CS_LOW_CMPVAL 122

typedef void (*stopwatch_callback_t)(void);

typedef struct {
    double time;
    stopwatch_callback_t callback;
    volatile uint8_t status;
} timer_t;

typedef enum {
    STOPWATCH_INITIALIZED,
    STOPWATCH_STARTED,
    STOPWATCH_NONE
} stopwatch_status_t;

/**
 * Start the stopwatch
 */
void stopwatch_start (void);

/**
 * Add a timer callback to the stopwatch
 * @param  time     The duration
 * @param  callback The callback function
 * @return          A pointer to the timer
 */
timer_t *stopwatch_add (
    double time,
    stopwatch_callback_t callback
);

/**
 * Remove a timer callback from the stopwatch
 * @param timer The timer to remove
 * @return      If successful
 */
uint8_t stopwatch_remove (timer_t *timer);

/**
 * Enable the power-saving mode
 */
void stopwatch_enable_ps (void);

/**
 * Disable the power-saving mode
 */
void stopwatch_disable_ps (void);

/**
 * Get the current time from the stopwatch
 * @return  The time elapsed
 */
double stopwatch_time (void);

/**
 * Stop and reset the stopwatch
 * @return  The time elapsed
 */
double stopwatch_stop (void);

#endif
